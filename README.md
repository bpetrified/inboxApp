# InboxApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.3.
To be used as assignment for SCB

- Responsive design
- Filter available
- High performance list with virtual scroll
- Interval fetch data (but constantly pointing to the same local JSON file for demo)
- Reusable components
- Unit test
- Error handling 

# Install

npm i

# Run

npm start

# Test

npm test

