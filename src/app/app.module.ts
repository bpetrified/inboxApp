import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { appRoutes } from './app.route';
import { SharedModule } from './shared'
//feature
import { MailModule } from './mail/mail.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes, { useHash: true }),   
    SharedModule,
    MailModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
