import { Routes } from '@angular/router';

export const appRoutes: Routes = [
    { path: '',  redirectTo: 'mail', pathMatch: 'full' },
    { path: '**', redirectTo: 'mail', pathMatch: 'full'  }
];
