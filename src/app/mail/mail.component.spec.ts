import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { MailComponent } from './mail.component';
import { DataService, MockDataService } from '../shared';
import { of, Observable } from 'rxjs';

describe('MailComponent', () => {
  let component: MailComponent;
  let fixture: ComponentFixture<MailComponent>;
  let dataService: MockDataService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailComponent ],
      schemas:[NO_ERRORS_SCHEMA],
      providers:[{provide: DataService, useClass:MockDataService }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailComponent);
    component = fixture.componentInstance;
    dataService = TestBed.get(DataService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onInit should start get data interval and subscribe to $', () => {
    //given
    spyOn(dataService, 'getDataIntervalStart');
    spyOn(component, 'subscribeToData$');
    spyOn(component, 'subscribeTorender$');
    //when
    component.ngOnInit();
    //expect
    expect(dataService.getDataIntervalStart).toHaveBeenCalled();
    expect(component.subscribeToData$).toHaveBeenCalled();
    expect(component.subscribeTorender$).toHaveBeenCalled();
  });

  it('on component destroyed should unsubscribe', () => {
    //given
    spyOn(dataService, 'unsubscribe').and.returnValue({});
    //when
    component.ngOnDestroy();
    //expect
    expect(dataService.unsubscribe).toHaveBeenCalled();
  });

  describe('subscribeToData$ func', () => {
    it('should emit value to mail$ and render$ when receive data', () => {
      //given
      const mockData = 'some data';
      dataService.data$ = of(mockData);
      spyOn(component.mail$, 'next');
      spyOn(component.render$, 'next');
      //when
      component.subscribeToData$();
      //expect
      expect(component.mail$.next).toHaveBeenCalledWith(mockData);
      expect(component.render$.next).toHaveBeenCalledWith(component.render$.value);
    });
  });

  describe('subscribeToRender$ func', () => {
    it('should do filter correctly and emit filter result', fakeAsync(() => {
      //given
      const mockFilter = {name: 'name'};
      const mockMails = [{name: 'name1'}];
      const spy = spyOn(component,<any>'_doFilter').and.callThrough();
      spyOn(component.filteredMail$, 'next').and.callThrough();
      component.mail$.next(<any>mockMails);
      //when
      component.subscribeTorender$();
      //when
      component.render$.next(mockFilter);
      //expect
      expect(spy).toHaveBeenCalledWith(component.mail$.value, mockFilter);
      tick();
      expect(component.filteredMail$.value).toEqual(<any>mockMails);

      //given
      component.mail$.next(<any>[{name: 'something not n@me'}]);
      //when
      component.render$.next(mockFilter);
      tick();
      expect(component.filteredMail$.value).toEqual([]);
    }));
  });
});
