import { Component, OnInit, OnDestroy, } from '@angular/core';
import { Observable, BehaviorSubject, Subject, ReplaySubject } from 'rxjs';
import { DataService, MailModel } from '../shared';
import { switchMap, map, flatMap } from 'rxjs/operators';
import { debounceTime } from 'rxjs/operators';
import * as _ from 'lodash';

@Component({
  selector: 'mail',
  templateUrl: './mail.component.html',
  styleUrls: ['./mail.component.scss']
})
export class MailComponent implements OnInit, OnDestroy {

  constructor(private dataService: DataService) { }

  title: string = 'Inbox';
  mail$: BehaviorSubject<MailModel[]> = new BehaviorSubject([]);
  filteredMail$: BehaviorSubject<MailModel[]> = new BehaviorSubject([]);
  render$: BehaviorSubject<any> = new BehaviorSubject({})

  ngOnInit() {
    this.dataService.getDataIntervalStart();
    this.subscribeTorender$();
    this.subscribeToData$();
  }

  ngOnDestroy() {
    this.dataService.unsubscribe();
  }

  onFilterChanged(filter) {
    this.render$.next(filter);
  }

  subscribeToData$() {
    this.dataService.data$.subscribe((datas) => {
      this.mail$.next(datas);
      this.render$.next(this.render$.value);
    });
  }

  subscribeTorender$() {
    this.render$
      .pipe(switchMap<any, MailModel[]>((filter) => {
        return new Observable((observer) => {
          observer.next(this._doFilter(this.mail$.value, filter));
        });
      }))
      .subscribe((result) => {
        this.filteredMail$.next(result);
      });
  }

  private _doFilter(items, filter) {
    let result = items;
    if (items.length > 0) {
      Object.keys(filter).forEach((key) => {
        if (items[0][key]) {
          result = result.filter(i => i[key].toLowerCase().includes(filter[key].toLowerCase()));
        }
      })
    }
    return result;
  }

}
