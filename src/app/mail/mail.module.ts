import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MailComponent } from './mail.component';
import { SharedModule } from '../shared';
import { mailRoutes } from './mail.route';

@NgModule({
  imports: [
    RouterModule.forRoot(mailRoutes, { useHash: true }),
    CommonModule,
    SharedModule
  ],
  declarations: [MailComponent]
})
export class MailModule { }
