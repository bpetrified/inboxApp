import { Routes } from '@angular/router';
import { MailComponent } from './mail.component';

export const mailRoutes: Routes = [{
   path: 'mail',
   component: MailComponent,
   pathMatch: 'full'
}];
