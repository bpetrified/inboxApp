import { animation, style, animate, trigger, transition, state } from "@angular/animations";

export const expandAnimation = animation([
  style({ height: 0 }),
  animate("{{time}}ms ease-in", style({ height: '*' }))
], { params: { time: 500 } });

export const collapseAnimation = animation([
  style({ height: '*' }),
  animate("{{time}}ms ease-in", style({ height: '0' }))
], { params: { time: 500 } });

