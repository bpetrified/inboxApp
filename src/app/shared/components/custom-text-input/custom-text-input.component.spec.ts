import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CustomTextInputComponent } from './custom-text-input.component';

describe('CustomTextInputComponent', () => {
  let component: CustomTextInputComponent;
  let fixture: ComponentFixture<CustomTextInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomTextInputComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomTextInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('writeValue(val) should assign to _value if val exist',() => {
    //given
    const mockVal = 'value';
    //when
    component.writeValue(mockVal);
    //expect
    expect(component._value).toEqual(mockVal);
  });

  it('registerOnChange should assign value to propagate func', () => {
    //given
    const mockHandler = 'some handler';
    //when
    component.registerOnChange(mockHandler);
    //expect
    expect(component.propagateChange).toEqual(mockHandler);
  });

  it('onChange should call propagateChange and emit value', () => {
    //given
    const mockEvent = { target: { value: 'some value '}}
    spyOn(component, 'propagateChange');
    spyOn(component.onTextChanged, 'emit');
    //when
    component.onChange(mockEvent);
    //expect
    expect(component.propagateChange).toHaveBeenCalledWith(mockEvent.target.value);
    expect(component.onTextChanged.emit).toHaveBeenCalledWith(mockEvent.target.value);
  });
});
