import { Component, OnInit, forwardRef, Input, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

const customValueProvider = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => CustomTextInputComponent),
  multi: true
};

@Component({
  selector: 'custom-text-input',
  templateUrl: './custom-text-input.component.html',
  styleUrls: ['./custom-text-input.component.scss'],
  providers: [customValueProvider]
})

//also support angular's form
export class CustomTextInputComponent implements OnInit, ControlValueAccessor {
  @Input() placeholder: string;
  @Output() onTextChanged: EventEmitter<string> = new EventEmitter();
  _value;

  constructor() { }

  ngOnInit() {
  }

  propagateChange: any = () => { };

  writeValue(value: any) {
    if (value) {
      this._value = value;
    }
  }

  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => void): void { }

  onChange(event) {
    this.propagateChange(event.target.value);
    this.onTextChanged.emit(event.target.value);
  }

}

