import { NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ListheaderComponent } from './listheader.component';

describe('ListheaderComponent', () => {
  //testIDs
  const LISTHEADER_TITLE_ID = 'listheader-title';

  let component: ListheaderComponent;
  let fixture: ComponentFixture<ListheaderComponent>;
  let listHeaderEL;
  let listHeaderDE;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListheaderComponent ],
      schemas:[NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    listHeaderEL = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('title should be rendered correctly', () => {
    //given
    const mockTitle = 'title';
    component.title = mockTitle;
    fixture.detectChanges();
    expect(listHeaderEL.querySelector(`#${LISTHEADER_TITLE_ID}`).textContent).toEqual(mockTitle);
  });
});
