import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { style, animate, trigger, transition, useAnimation } from '@angular/animations';
import { expandAnimation, collapseAnimation } from '../../animations/expandable.animation';

@Component({
  selector: 'listheader',
  templateUrl: './listheader.component.html',
  styleUrls: ['./listheader.component.scss'],
  animations: [trigger('expandable', [
    transition(':enter', [
      expandAnimation
    ]),
    transition(':leave', [
      collapseAnimation
    ])
  ])]
})
export class ListheaderComponent implements OnInit {
  @Input() title: string;
  @Input() filterKeys: string[] = [];
  @Output() onFilterChanged: EventEmitter<any> = new EventEmitter();

  filterShown: boolean = false;
  filter = {};

  constructor() { }

  ngOnInit() {

  }

  toggleFilter() {
    this.filterShown = !this.filterShown;
  }

  _onFilterChanged() {
    this.onFilterChanged.emit(this.filter);
  }
}
