import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ListitemComponent } from './listitem.component';
import { CustomTimePipe } from '../../../shared';
import { By } from '@angular/platform-browser';

describe('ListitemComponent', () => {
  //test ID
  const LISTITEM_AVATAR_ID = 'listitem-avatar';
  const LISTITEM_NAME_ID = 'listitem-name';
  const LISTITEM_SUBJECT_ID = 'listitem-subject';
  const LISTITEM_BODY_ID = 'listitem-body';

  const mockItem = { name: 'name', subject: 'subject', body: 'body' };

  let component: ListitemComponent;
  let fixture: ComponentFixture<ListitemComponent>;
  let listItemEL;
  let listItemDE;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListitemComponent, CustomTimePipe],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListitemComponent);
    component = fixture.componentInstance;
    component.item = mockItem;
    fixture.detectChanges();
    listItemEL = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render correctly', () => {
    expect(listItemEL.querySelector(`#${LISTITEM_NAME_ID}`).textContent).toEqual(mockItem.name);
    expect(listItemEL.querySelector(`#${LISTITEM_SUBJECT_ID}`).textContent).toEqual(mockItem.subject);
    expect(listItemEL.querySelector(`#${LISTITEM_BODY_ID}`).textContent).toEqual(mockItem.body);
  });
});
