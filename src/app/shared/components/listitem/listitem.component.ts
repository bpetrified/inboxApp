import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'listitem',
  templateUrl: './listitem.component.html',
  styleUrls: ['./listitem.component.scss']
})
export class ListitemComponent implements OnInit {
  @Input() item = {};

  constructor() { }

  ngOnInit() {

  }

}
