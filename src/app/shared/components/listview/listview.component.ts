import { Component, OnInit, ContentChild, TemplateRef, Input } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'listview',
  templateUrl: './listview.component.html',
  styleUrls: ['./listview.component.scss']
})
export class ListviewComponent implements OnInit {
  @Input() template: TemplateRef<any>;
  @Input('empty-template') emptyTemplate: TemplateRef<any>;
  @Input() items: Observable<any>;

  constructor() { }

  ngOnInit() {
  }

}
