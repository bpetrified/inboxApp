import { RotateDirective } from './rotate.directive';
import { Directive, ElementRef, Input } from '@angular/core';
import { AnimationBuilder } from '@angular/animations';

describe('RotateDirective', () => {
  it('should create an instance', () => {
    const directive = new RotateDirective({} as AnimationBuilder, {} as ElementRef);
    expect(directive).toBeTruthy();
  });
});
