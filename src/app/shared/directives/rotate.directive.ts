import { Directive, ElementRef, Input } from '@angular/core';
import { AnimationBuilder, AnimationPlayer, AnimationMetadata, style, animate } from '@angular/animations';

@Directive({
  selector: '[rotate]'
})
export class RotateDirective {

  constructor(private builder: AnimationBuilder, private el: ElementRef) { }

  player: AnimationPlayer;

  @Input() duration: number = 400;
  @Input() degree: number = 180;

  @Input()
  set rotate(rotate: boolean) {
    if (this.player) {
      this.player.destroy();
    }
    const metadata = rotate ? this.do() : this.undo();

    const factory = this.builder.build(metadata);
    const player = factory.create(this.el.nativeElement);

    player.play();
  }

  private do(): AnimationMetadata[] {
    return [
      style({ transform: '*' }),
      animate(`${this.duration}ms ease-in`, style({ transform: `rotate(${this.degree}deg)` })),
    ];
  }

  private undo(): AnimationMetadata[] {
    return [
      style({ transform: '*' }),
      animate(`${this.duration}ms ease-in`, style({ transform: 'rotate(0)' })),
    ];
  }

}
