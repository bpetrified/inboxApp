import { Subscriber } from "rxjs";

export interface IhttpError {
    status: number;
    message: string;
}

export interface IhttpErrorHandler {
    (err: IhttpError, observer: Subscriber<any>): any
}