import { mapChildrenIntoArray } from "@angular/router/src/url_tree";
import { get } from 'lodash';

export class MailModel {
    name: string;
    email: string;
    subject: string;
    body: string;
    date: number;
    constructor(
        mailDTO: ImailDTO
    ) {
        this.name = get(mailDTO, 'from.name','unknown');
        this.email = get(mailDTO, 'from.email', '');
        this.subject = mailDTO.subject;
        this.body = mailDTO.body;
        this.date = mailDTO.date;
    }

    static of(mailDTO: ImailDTO) {
        return new MailModel(mailDTO);
    }
}

export interface ImailDTO {
    from: {
        name: string,
        email: string
    };
    subject: string,
    body: string,
    date: number
}