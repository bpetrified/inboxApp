import { MailModel, ImailDTO } from '../../../shared';

describe('MailModel', () => {
    it('should map correct value from Mail DTO', () => {
        //given
        const mailDTO:ImailDTO = {
            from: {
                name: 'name',
                email: 'email'
            },
            subject: 'subject',
            body: 'body',
            date: 12345678
        };
        //when
        const mail = MailModel.of(mailDTO);
        //expect
        expect(mail.name).toEqual(mailDTO.from.name);
        expect(mail.email).toEqual(mailDTO.from.email);
        expect(mail.subject).toEqual(mailDTO.subject);
        expect(mail.body).toEqual(mailDTO.body);
        expect(mail.date).toEqual(mail.date);
    });
}); 