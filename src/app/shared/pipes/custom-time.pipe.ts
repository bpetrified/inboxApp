import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'customTime'
})
export class CustomTimePipe implements PipeTransform {
  transform(value: any, args?: any): any {
    return Number.isInteger(value) ? moment(value).calendar(null, {
      sameDay: 'hh:mm',
      lastDay: 'DD/MM/YYYY',
      lastWeek: 'DD/MM/YYYY',
      sameElse: 'DD/MM/YYYY'
    }) : '';
  }
}
