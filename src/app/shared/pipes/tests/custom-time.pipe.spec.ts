import { CustomTimePipe } from '../custom-time.pipe';
import * as moment from 'moment';

describe('CustomTimePipe', () => {
  let pipe: CustomTimePipe;

  beforeEach(() => {
    pipe = new CustomTimePipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should transform date correctly', () => {
      let time1 = new Date(2001, 2, 1, 9,59).getTime();
      expect(pipe.transform(time1)).toEqual(moment(time1).format('DD/MM/YYYY'));

      time1 = new Date().getTime();
      expect(pipe.transform(time1)).toEqual(moment(time1).format('hh:mm'));
  });
});
