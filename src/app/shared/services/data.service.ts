import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { Observable, interval, pipe, timer, Subscription } from 'rxjs';
import { switchMap, map, flatMap } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { HttpService, IhttpError, MailModel, IhttpErrorHandler, ImailDTO } from '../../shared';
import { APP_CONFIG } from '../../app.constant';
@Injectable()
export class DataService implements OnDestroy {
    private _dataChanged$ = new ReplaySubject<MailModel[]>(1);
    private _intervalSubscription: Subscription;

    constructor(
        private http: HttpService
    ) {
    }

    get data$(): Observable<MailModel[]> {
        return this._dataChanged$.asObservable();
    }

    getDataIntervalStart(intervalTime: number = APP_CONFIG.INTERVAL_TIME) {
        this._intervalSubscription = timer(0, intervalTime).pipe(switchMap((() => {

            let randomNo;
            //put some logic here to illustrate the error handling mechanism, noted that uncomment the line below will break unittest
            //randomNo = Math.floor(Math.random() * 2) + 0; //0 = ok with empty array, 1 = 404 error
            return this.http.get(`${APP_CONFIG.DATA_PATH}data${randomNo || ''}.json`, getDataIntervalErrorHandler);
        }))).pipe(map((mailDTOs: ImailDTO[]) => {
            return mailDTOs.map((mailDTO) => MailModel.of(mailDTO))
        })).subscribe((mails) => {
            this._dataChanged$.next(mails);
        });

        const getDataIntervalErrorHandler: IhttpErrorHandler = (err) => {
            console.log(`fetch data fail (${err.status}) with message:${err.message} | .... continue interval ....`);
        }
    }

    ngOnDestroy() {
        this.unsubscribe();
    }

    unsubscribe(){
        if(this._intervalSubscription){
            this._intervalSubscription.unsubscribe();
        }
    }
}