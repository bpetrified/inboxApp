import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import { IhttpError, IhttpErrorHandler } from '../../shared';

@Injectable()
export class HttpService {
    constructor(
        private http: HttpClient
    ) {
    }

    get(url,
        onError?: IhttpErrorHandler//you may need to intercept the error different way
    ): Observable<any> {
        return new Observable((observer) => {
            this.http.get(url).subscribe((resp) => {
                observer.next(resp);
            }, onError ? (err) => { onError(err, observer) } :
                    (err) => { this._errorHandler(err, observer); }
                , () => { observer.complete(); })
        });
    }

    private _errorHandler(err, observer): Observable<IhttpError> {
        return observer.error({
            status: err.status,
            message: err.message
        });
    }
}