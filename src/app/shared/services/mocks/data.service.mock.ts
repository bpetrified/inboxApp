import { Observable } from 'rxjs';

export class MockDataService {
    data$ = new Observable();
    getDataIntervalStart(){}
    unsubscribe(){}
}