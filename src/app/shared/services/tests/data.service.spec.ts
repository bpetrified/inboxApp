import { TestBed, async, fakeAsync, tick, getTestBed, inject } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { DataService, HttpService } from '../../../shared';
import { Observable, of } from 'rxjs';
import { APP_CONFIG } from '../../../app.constant';

describe('DataService', () => {
    let service: DataService;
    let httpService: HttpService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            providers: [DataService, {provide: HttpService, useValue: {get(){}}}]
        });
        service = TestBed.get(DataService);
        httpService = TestBed.get(HttpService);
    }));

    describe('getDataInterval func', () => {
        it('should call http service in the correct timing', fakeAsync(() => {
            //given
            spyOn(httpService, 'get').and.returnValue(of([]));
            service.getDataIntervalStart();
            tick(0);
            expect(httpService.get).toHaveBeenCalledTimes(1);
            tick(APP_CONFIG.INTERVAL_TIME);
            expect(httpService.get).toHaveBeenCalledTimes(2);
            tick(APP_CONFIG.INTERVAL_TIME);
            expect(httpService.get).toHaveBeenCalledTimes(3);
            service.ngOnDestroy();
        }));

        it('on http error, should continue http service call', fakeAsync(()=>{
            //given
            TestBed.resetTestingModule();
            //we need to interact with httpClient to simulate error
            TestBed.configureTestingModule({
                providers: [DataService, HttpService, {provide:HttpClient, useValue: {get(){}}} ]
            });
            service = TestBed.get(DataService);
            httpService = TestBed.get(HttpService);
            let http = TestBed.get(HttpClient);

            spyOn(httpService, 'get').and.callThrough();
            spyOn(http, 'get').and.returnValue(Observable.throw(''))
            
            service.getDataIntervalStart();
            tick(0);
            expect(httpService.get).toHaveBeenCalledTimes(1);
            tick(APP_CONFIG.INTERVAL_TIME);
            expect(httpService.get).toHaveBeenCalledTimes(2);
            service.ngOnDestroy();
        }))
    });
})