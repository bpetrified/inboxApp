import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../../../shared';

describe('HttpService', () => {
    let service: HttpService;
    let http: HttpTestingController;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [HttpService]
        });
        service = TestBed.get(HttpService);
        http = TestBed.get(HttpTestingController);
    }));

    afterEach(() => {
        http.verify();
    });

    describe('get func', () => {
        it('should make http call and handle resp', () => {
            const mockResp = 'some resp';
            const mockPath = 'some path';
            let resp;
            service.get(mockPath).subscribe((_resp) => {
                resp = _resp;
            });
            http.expectOne(mockPath).flush(mockResp);
            expect(resp).toBe(mockResp);
        });

        it('should call passed error handler if exists when http error', () => {
            const mockPath = 'some path';
            const mockErrorHandler = jasmine.createSpy('errorHandler');
            service.get(mockPath, mockErrorHandler).subscribe((_resp) => {});
            http.expectOne(mockPath).error(new ErrorEvent(''));
            expect(mockErrorHandler).toHaveBeenCalled();
        });
    });
});
