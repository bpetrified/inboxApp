import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
//libs
import { FlexLayoutModule } from '@angular/flex-layout';
import { VirtualScrollModule } from 'angular2-virtual-scroll';
import { AvatarModule } from 'ngx-avatar';

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        CommonModule,
        FormsModule,
        HttpClientModule,
        VirtualScrollModule,
        FlexLayoutModule,
        AvatarModule
    ],
    providers: [
        
    ],
    declarations: [

    ],
    exports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        BrowserModule,
        BrowserAnimationsModule,
        FlexLayoutModule,
        VirtualScrollModule,
        AvatarModule
    ]
})

export class SharedLibsModule { }
