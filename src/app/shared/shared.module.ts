import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedLibsModule } from './shared-lib.module';
import {
    ListheaderComponent, ListviewComponent, ListitemComponent,
    CustomTextInputComponent, CustomTimePipe, HttpService, DataService
} from '../shared';

@NgModule({
    imports: [
        SharedLibsModule
    ],
    declarations: [ListheaderComponent, ListviewComponent, ListitemComponent, CustomTimePipe,
        CustomTextInputComponent],
    providers: [
        HttpService, DataService
    ],
    entryComponents: [],
    exports: [
        SharedLibsModule,
        ListitemComponent,
        ListviewComponent,
        ListheaderComponent,
        CustomTimePipe,
        CustomTextInputComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]

})

export class SharedModule {
}
